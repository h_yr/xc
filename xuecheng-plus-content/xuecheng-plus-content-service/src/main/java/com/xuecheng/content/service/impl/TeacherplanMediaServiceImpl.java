package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.content.mapper.TeachplanMediaMapper;
import com.xuecheng.content.model.po.TeachplanMedia;
import com.xuecheng.content.service.TeacherplanMediaService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherplanMediaServiceImpl extends ServiceImpl<TeachplanMediaMapper, TeachplanMedia> implements TeacherplanMediaService {

    @Autowired
    private TeachplanMediaMapper teachplanMediaMapper;

    /**
     * 移除课程计划关联的媒资信息
     *
     * @param teachPlanId 课程计划id
     * @param mediaId     媒资id
     */
    @Override
    public void removeAssociationMedia(String teachPlanId, String mediaId) {
        // 根据课程计划id查询课程计划关联的媒资信息
        LambdaQueryWrapper<TeachplanMedia> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(StringUtils.isNotEmpty(teachPlanId), TeachplanMedia::getTeachplanId, teachPlanId)
                .eq(StringUtils.isNotEmpty(mediaId), TeachplanMedia::getMediaId, mediaId);
        TeachplanMedia teachplanMedia = teachplanMediaMapper.selectOne(queryWrapper);
        if (teachplanMedia == null) XueChengPlusException.cast("获取媒资信息失败");
        // 移除课程计划关联的媒资信息
        int deleted = teachplanMediaMapper.delete(queryWrapper);
        if (deleted != 1) XueChengPlusException.cast("移除媒资信息失败");
    }
}
