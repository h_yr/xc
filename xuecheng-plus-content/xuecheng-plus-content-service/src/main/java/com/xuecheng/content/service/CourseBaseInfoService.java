package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.content.model.dto.AddCourseDto;
import com.xuecheng.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.content.model.dto.EditCourseDto;
import com.xuecheng.content.model.dto.QueryCourseParamsDto;
import com.xuecheng.content.model.po.CourseBase;

/**
 * 课程基本信息管理业务接口
 */
public interface CourseBaseInfoService extends IService<CourseBase> {

    /**
     * 课程查询接口
     *
     * @param pageParams        分页参数
     * @param queryCourseParams 条件参数
     * @return
     */
    PageResult<CourseBase> queryCourseBaseList(Long companyId, PageParams pageParams, QueryCourseParamsDto queryCourseParams);

    /**
     * 添加课程基本信息
     *
     * @param companyId    教育机构id
     * @param addCourseDto 课程基本信息
     * @return
     */
    CourseBaseInfoDto createCourseBase(Long companyId, AddCourseDto addCourseDto);

    /**
     * 根据课程id查询课程基础信息
     *
     * @param courseId 课程id
     * @return
     */
    CourseBaseInfoDto getCourseBaseById(Long courseId);

    /**
     * 修改课程信息
     *
     * @param editCourseDto 课程信息
     * @return
     */
    CourseBaseInfoDto updateCourseBase(EditCourseDto editCourseDto);

    /**
     * 删除课程信息
     *
     * @param companyId 机构id
     * @param courseId  课程id
     */
    void removeCourse(Long companyId, Long courseId);

}
