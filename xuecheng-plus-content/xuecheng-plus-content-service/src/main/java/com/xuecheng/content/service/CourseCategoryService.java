package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.content.model.dto.CourseCategoryTreeDto;
import com.xuecheng.content.model.po.CourseCategory;

import java.util.List;

/**
 * 课程查询业务接口
 */
public interface CourseCategoryService extends IService<CourseCategory> {
    // 查询根节点
    List<CourseCategoryTreeDto> queryTreeNodes(String id);
}
