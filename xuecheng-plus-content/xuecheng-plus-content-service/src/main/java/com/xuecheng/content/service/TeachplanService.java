package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;
import com.xuecheng.content.model.po.TeachplanMedia;

import java.util.List;

/**
 * 课程基本信息管理业务接口
 */
public interface TeachplanService extends IService<Teachplan> {
    /**
     * 查询课程计划树型结构
     *
     * @param courseId 课程id
     * @return
     */
    List<TeachplanDto> findTeachplanTree(Long courseId);

    /**
     * 保存课程计划
     *
     * @param saveTeachplanDto 课程计划dto
     */
    void saveTeachplan(SaveTeachplanDto saveTeachplanDto);

    /**
     * 删除课程计划
     *
     * @param teachplanId 课程计划id
     */
    void removeTeachplan(Long teachplanId);

    /**
     * 课程计划排序（上下移动）
     *
     * @param moveType    移动类型
     * @param teachplanId 课程计划id
     */
    void orderByTeachplan(String moveType, Long teachplanId);

    /**
     * 教学计划绑定媒资
     *
     * @param bindTeachplanMediaDto 教学计划-媒资绑定提交数据
     */
    void associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto);
}
