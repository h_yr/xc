package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.content.model.po.TeachplanMedia;

public interface TeacherplanMediaService extends IService<TeachplanMedia> {
    /**
     * 移除课程计划关联的媒资信息
     *
     * @param teachPlanId 课程计划id
     * @param mediaId     媒资id
     */
    void removeAssociationMedia(String teachPlanId, String mediaId);
}
