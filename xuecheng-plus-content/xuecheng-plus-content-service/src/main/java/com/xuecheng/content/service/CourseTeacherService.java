package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.content.model.po.CourseTeacher;

import java.util.List;

public interface CourseTeacherService extends IService<CourseTeacher> {

    // 查询教师列表信息
    List<CourseTeacher> findTeacherList(Long courseId);

    // 添加/修改教师信息
    CourseTeacher saveCourseTeacher(CourseTeacher courseTeacher);

    // 删除教师信息
    void deleteCourseTeacher(Long courseId, Long teacherId);
}
