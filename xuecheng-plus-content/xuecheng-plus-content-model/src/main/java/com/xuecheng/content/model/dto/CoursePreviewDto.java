package com.xuecheng.content.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@ApiModel(description = "课程预览数据模型")
@Data
@ToString
public class CoursePreviewDto {

    @ApiModelProperty(value = "课程基本信息,课程营销信息")
    private CourseBaseInfoDto courseBase;

    @ApiModelProperty(value = "课程计划信息")
    private List<TeachplanDto> teachplans;
}
