package com.xuecheng.content.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * 注解：@EnableResourceServer 用于启用资源服务器，它告诉Spring Boot应用程序将充当OAuth2资源服务器。
 * 资源服务器是一个受保护的API，它需要验证和授权才能访问。
 * -----------------------------------------------
 * 注解：@EnableGlobalMethodSecurity 用于启用全局方法安全性，它允许在方法级别上进行安全性控制。
 * securedEnabled = true表示启用@Secured注解，prePostEnabled = true表示启用@PreAuthorize和@PostAuthorize注解。
 * 注解@PreAuthorize 用于在方法执行之前进行权限验证。
 * 注解@PostAuthorize 用于在方法执行之后进行权限验证。
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    // 资源服务标识
    public static final String RESOURCE_ID = "xuecheng-plus";

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID) // 资源id
                .tokenStore(tokenStore)
                .stateless(true);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .anyRequest().permitAll();  // 请求一律放行（由网关统一进行认证，资源这边只进行授权操作）
    }

}
