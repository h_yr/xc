package com.xuecheng.checkcode.utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * 工具类
 * 用于发送邮件验证码
 */
public class MailUtils {
    public static void main(String[] args) throws MessagingException {
        // 可以在这里直接测试方法，填自己的邮箱即可
        sendTestMail("81674944@qq.com", new MailUtils().achieveCode());
    }

    /**
     * 发送验证码
     *
     * @param email 收件人的邮箱
     * @param code  发送的验证码
     * @throws MessagingException 发送验证码异常
     */
    public static void sendTestMail(String email, String code) throws MessagingException {
        // 创建Properties 类用于记录邮箱的一些属性
        Properties props = new Properties();
        // 表示SMTP发送邮件，必须进行身份验证
        props.put("mail.smtp.auth", "true");
        // 此处填写SMTP服务器
        props.put("mail.smtp.host", "smtp.qq.com");
        // 端口号，QQ邮箱端口587
        props.put("mail.smtp.port", "587");
        // 此处填写，写信人的账号
        props.put("mail.user", "81674944@qq.com");
        // 此处填写16位STMP口令
        props.put("mail.password", "uthlkcqthuaccbeb");
        // 构建授权信息，用于进行SMTP进行身份验证
        Authenticator authenticator = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                // 用户名、密码
                String userName = props.getProperty("mail.user");
                String password = props.getProperty("mail.password");
                return new PasswordAuthentication(userName, password);
            }
        };
        // 使用环境属性和授权信息，创建邮件会话
        Session mailSession = Session.getInstance(props, authenticator);
        // 创建邮件消息
        MimeMessage message = new MimeMessage(mailSession);
        // 设置发件人
        InternetAddress form = new InternetAddress(props.getProperty("mail.user"));
        message.setFrom(form);
        // 设置收件人的邮箱
        InternetAddress to = new InternetAddress(email);
        message.setRecipient(RecipientType.TO, to);
        // 设置邮件标题
        message.setSubject("学成课堂 邮件");
        // 设置邮件的内容体
        message.setContent("[学成课堂]:验证码 " + code + " (3分钟内有效，请勿告知他人)", "text/html;charset=UTF-8");
        // 最后发送邮件
        Transport.send(message);
    }

    /**
     * 随机生成验证码
     *
     * @return 验证码
     */
    public static String achieveCode() {
        // 由于数字 1 、 0 和字母 O 、l 有时分不清楚，所以，没有数字 1 、 0
        String[] beforeShuffle = new String[]
                {"2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F",
                        "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a",
                        "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                        "w", "x", "y", "z"};
        // 将数组转换为集合
        List<String> list = Arrays.asList(beforeShuffle);
        // 打乱集合顺序
        Collections.shuffle(list);
        StringBuilder sb = new StringBuilder();
        // 将集合转化为字符串
        for (String s : list) {
            sb.append(s);
        }
        return sb.substring(3, 8);
    }
}
