package com.xuecheng.checkcode.service.impl;

import com.xuecheng.checkcode.service.CheckCodeService;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * 数字字母生成器
 */
@Component("NumberLetterCheckCodeGenerator")
public class NumberLetterCheckCodeGenerator implements CheckCodeService.CheckCodeGenerator {

    @Override
    public String generate(int length) {
        // 验证码字符
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        // 随机数对象
        Random random = new Random();
        // 动态字符串
        StringBuffer sb = new StringBuffer();
        // 生成指定位数的验证码
        for (int i = 0; i < length; i++) {
            // 生成一个介于0和36之间（不包括36）的随机数
            int number = random.nextInt(36);
            // 往动态字符串中追加指定下标的字符
            sb.append(str.charAt(number));
        }
        // 返回生成好的验证码
        return sb.toString();
    }

}
