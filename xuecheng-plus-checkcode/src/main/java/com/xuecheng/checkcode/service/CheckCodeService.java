package com.xuecheng.checkcode.service;

import com.xuecheng.checkcode.model.CheckCodeParamsDto;
import com.xuecheng.checkcode.model.CheckCodeResultDto;

/**
 * 验证码接口
 */
public interface CheckCodeService {

    /**
     * 生成验证码
     *
     * @param checkCodeParamsDto 生成验证码参数
     * @return 验证码结果
     */
    CheckCodeResultDto generate(CheckCodeParamsDto checkCodeParamsDto);

    /**
     * 校验验证码
     *
     * @param key  键
     * @param code 验证码
     * @return 验证成功：true，验证失败：false
     */
    boolean verify(String key, String code);


    /**
     * 验证码生成器
     */
    interface CheckCodeGenerator {
        /**
         * 验证码生成
         *
         * @return 验证码
         */
        String generate(int length);

    }

    /**
     * key生成器
     */
    interface KeyGenerator {

        /**
         * key生成
         *
         * @return 验证码
         */
        String generate(String prefix);
    }


    /**
     * 验证码存储
     */
    interface CheckCodeStore {
        /**
         * 向redis缓存设置key
         *
         * @param key    键
         * @param value  值
         * @param expire 过期时间
         */
        void set(String key, String value, Integer expire);

        /**
         * 从redis缓存中获取验证码
         *
         * @param key 键
         * @return 验证码
         */
        String get(String key);

        /**
         * 从redis缓存中移除验证码存储信息
         *
         * @param key 键
         */
        void remove(String key);
    }
}
