package com.xuecheng.checkcode.service;


import com.xuecheng.checkcode.model.CheckCodeResultDto;

public interface SendCodeService {

    /**
     * 发送邮件验证码
     *
     * @param email 邮箱号
     * @param code  验证码
     * @return 验证码返回结果嘞
     */
    CheckCodeResultDto sendCode(String email, String code);
}
