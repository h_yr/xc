package com.xuecheng.checkcode.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * kaptcha图片验证码配置类
 */
@Configuration
public class KaptchaConfig {

    // 图片验证码生成器，使用开源的kaptcha
    @Bean
    public DefaultKaptcha producer() {
        // 创建Properties属性对象，用于存储kaptcha库的配置属性
        Properties properties = new Properties();
        // 边框样式
        properties.put("kaptcha.border", "no");
        // 字体颜色
        properties.put("kaptcha.textproducer.font.color", "black");
        // 字符间距
        properties.put("kaptcha.textproducer.char.space", "10");
        // 字符长度（验证码长度）
        properties.put("kaptcha.textproducer.char.length", "4");
        // 图片高度
        properties.put("kaptcha.image.height", "34");
        // 图片宽度
        properties.put("kaptcha.image.width", "138");
        // 字体大小
        properties.put("kaptcha.textproducer.font.size", "25");
        // 噪声实现
        properties.put("kaptcha.noise.impl", "com.google.code.kaptcha.impl.NoNoise");
        // 创建Config对象，传入刚刚设置好的Properties属性对象
        Config config = new Config(properties);
        // 创建DefaultKaptcha返回对象
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        // 设置配置
        defaultKaptcha.setConfig(config);
        // 返回结果
        return defaultKaptcha;
    }
}
