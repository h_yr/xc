package com.xuecheng.media.api;

import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.media.model.dto.QueryMediaParamsDto;
import com.xuecheng.media.model.dto.UploadFileParamsDto;
import com.xuecheng.media.model.po.MediaFiles;
import com.xuecheng.media.service.MediaFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 媒资文件管理接口
 */
@Api(value = "媒资文件管理接口", tags = "媒资文件管理接口")
@RestController
public class MediaFilesController {

    @Autowired
    MediaFileService mediaFileService;

    @ApiOperation("媒资列表查询接口")
    @PostMapping("/files")
    public PageResult<MediaFiles> list(PageParams pageParams, @RequestBody QueryMediaParamsDto queryMediaParamsDto) {
        Long companyId = 1232141425L;
        return mediaFileService.queryMediaFiels(companyId, pageParams, queryMediaParamsDto);
    }

    @ApiOperation("上传文件")
    @PostMapping("/upload/coursefile")
    public MediaFiles upload(MultipartFile filedata) {
        // 机构id
        Long companyId = 1232141425L;
        // dto
        UploadFileParamsDto dto = new UploadFileParamsDto();
        // 设置文件名称
        dto.setFilename(filedata.getOriginalFilename());
        // 设置文件类型（图片）
        dto.setFileType("001001");
        // 设置文件大小
        dto.setFileSize(filedata.getSize());

        // 获取临时本地文件路径
        String absolutePath = null;
        try {
            // 创建临时文件
            File tempFile = File.createTempFile("minio", ".temp");
            // 将MultipartyFile对象的数据存储到临时文件中
            filedata.transferTo(tempFile);
            // 获取临时文件的路径
            absolutePath = tempFile.getAbsolutePath();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // 执行上传操作，并返回给前端结果
        return mediaFileService.uploadFile(companyId, dto, absolutePath);

    }

}
