package com.xuecheng.media.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Minio配置类
 */
@Configuration
public class MinioConfig {

    // 连接地址
    @Value("${minio.endpoint}")
    private String endpoint;

    // 账号
    @Value("${minio.accessKey}")
    private String accessKey;

    // 密码
    @Value("${minio.secretKey}")
    private String secretKey;

    // 创建连接MinIO的客户端
    @Bean
    public MinioClient minioClient() {
        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint(endpoint)
                        .credentials(accessKey, secretKey)
                        .build();
        return minioClient;
    }

}
