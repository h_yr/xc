package com.xuecheng.media;

import io.minio.*;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;

public class MinioTest {

    // minio客户端
    private MinioClient minioClient;

    // 上传文件
    @Test
    void upload() throws Exception {
        // 上传文件所需参数
        UploadObjectArgs uploadObjectArgs = UploadObjectArgs.builder()
                .bucket("testbucket")
                .object("mario.png")
                .filename("C:\\Users\\81674\\Pictures\\Mario.png")
                .build();
        // 执行上传文件
        minioClient.uploadObject(uploadObjectArgs);
    }

    // 获取文件
    @Test
    void getFile() throws Exception {
        // 获取文件所需参数
        GetObjectArgs getObjectArgs = GetObjectArgs.builder()
                .bucket("testbucket")
                .object("mario.png")
                .build();
        // 执行获取文件
        FilterInputStream response = minioClient.getObject(getObjectArgs);
        // 创建文件输出流对象
        FileOutputStream fileOutputStream = new FileOutputStream(new File("C:\\Users\\81674\\Pictures\\Mario2.png"));
        // 文件拷贝
        IOUtils.copy(response, fileOutputStream);
    }

    // 删除文件
    @Test
    void remove() throws Exception {
        // 删除文件所需参数
        RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder()
                .bucket("testbucket")
                .object("mario.png")
                .build();
        // 执行删除文件
        minioClient.removeObject(removeObjectArgs);
    }
}
