package com.xuecheng.ucenter.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value = "注册参数dto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDto {

    @ApiModelProperty("手机号")
    private String cellphone;

    @ApiModelProperty("验证码")
    private String checkcode;

    @ApiModelProperty("验证码key")
    private String checkcodekey;

    @ApiModelProperty("确认密码")
    private String confirmpwd;

    @ApiModelProperty("邮箱号")
    private String email;

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("账号")
    private String username;

}