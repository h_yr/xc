package com.xuecheng.ucenter.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value = "找回密码参数dto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindPasswordDto {

    @ApiModelProperty("手机号")
    String cellphone;

    @ApiModelProperty("邮箱")
    String email;

    @ApiModelProperty("验证码")
    String checkcodekey;

    @ApiModelProperty("验证码")
    String checkcode;

    @ApiModelProperty("密码")
    String password;

    @ApiModelProperty("确认密码")
    String confirmpwd;
}
