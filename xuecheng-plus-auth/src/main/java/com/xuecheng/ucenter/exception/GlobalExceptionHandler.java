package com.xuecheng.ucenter.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 该异常枚举错误码为500
public class GlobalExceptionHandler {

    /**
     * ExceptionHandler(Exception.class) 处理异常
     *
     * @param e 注入异常对象
     * @return 响应用户的统一类型
     */
    @ExceptionHandler(Exception.class)
    public RestErrorResponse exception(Exception e) {
        return new RestErrorResponse(e.getMessage());
    }

}
