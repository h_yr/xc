package com.xuecheng.ucenter.service;

import com.xuecheng.ucenter.model.dto.FindPasswordDto;

public interface FindPwdService {

    /**
     * 找回密码
     *
     * @param findPasswordDto 找回密码表单数据
     * @return 是否成功
     */
    Boolean findpassword(FindPasswordDto findPasswordDto);
}
