package com.xuecheng.ucenter.service;

import com.xuecheng.ucenter.model.dto.RegisterDto;

public interface RegisterService {

    /**
     * 注册
     *
     * @param registerDto 注册参数dto
     */
    void register(RegisterDto registerDto);
}
