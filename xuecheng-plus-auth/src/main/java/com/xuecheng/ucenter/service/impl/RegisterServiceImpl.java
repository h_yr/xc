package com.xuecheng.ucenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xuecheng.ucenter.feignclient.CheckCodeClient;
import com.xuecheng.ucenter.mapper.XcUserMapper;
import com.xuecheng.ucenter.mapper.XcUserRoleMapper;
import com.xuecheng.ucenter.model.dto.RegisterDto;
import com.xuecheng.ucenter.model.po.XcUser;
import com.xuecheng.ucenter.model.po.XcUserRole;
import com.xuecheng.ucenter.service.RegisterService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private CheckCodeClient checkCodeClient;

    @Autowired
    private XcUserMapper xcUserMapper;

    @Autowired
    private XcUserRoleMapper xcUserRoleMapper;

    /**
     * 注册
     *
     * @param registerDto 注册参数dto
     */
    @Transactional
    @Override
    public void register(RegisterDto registerDto) {
        // UUID，用于生成主键
        String uuid = UUID.randomUUID().toString();
        // 邮箱号（验证码的key）
        String email = registerDto.getEmail();
        // 验证码
        String code = registerDto.getCheckcode();
        // 远程调用校验验证码
        Boolean verify = checkCodeClient.verify(email, code);
        // 验证码错误
        if (!verify) {
            throw new RuntimeException("验证码输入错误");
        }
        // 获取密码
        String password = registerDto.getPassword();
        // 获取验证密码
        String confirmPwd = registerDto.getConfirmpwd();
        // 两次输入的密码不一致
        if (!password.equals(confirmPwd)) {
            throw new RuntimeException("两次输入的密码不一致");
        }
        // 根据邮箱号查询用户
        LambdaQueryWrapper<XcUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(XcUser::getEmail, registerDto.getEmail());
        XcUser user = xcUserMapper.selectOne(lambdaQueryWrapper);
        // 用户已存在
        if (user != null) {
            throw new RuntimeException("用户已存在，一个邮箱只能注册一个账号");
        }
        // 用户不存在
        XcUser xcUser = new XcUser();
        BeanUtils.copyProperties(registerDto, xcUser);
        xcUser.setPassword(new BCryptPasswordEncoder().encode(password));
        xcUser.setId(uuid);
        xcUser.setUtype("101001");  // 学生类型
        xcUser.setStatus("1");      // 正常
        xcUser.setName(registerDto.getNickname());
        xcUser.setCreateTime(LocalDateTime.now());
        // 插入用户信息
        int insertUser = xcUserMapper.insert(xcUser);
        if (insertUser != 1) {
            throw new RuntimeException("新增用户信息失败");
        }
        // 插入用户角色信息
        XcUserRole xcUserRole = new XcUserRole();
        xcUserRole.setId(uuid);
        xcUserRole.setUserId(uuid);
        xcUserRole.setRoleId("17"); // 学生
        xcUserRole.setCreateTime(LocalDateTime.now());
        int insertUserRole = xcUserRoleMapper.insert(xcUserRole);
        if (insertUserRole != 1) {
            throw new RuntimeException("新增用户角色信息失败");
        }
    }
}
