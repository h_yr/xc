package com.xuecheng.auth.controller;

import com.xuecheng.ucenter.mapper.XcUserMapper;
import com.xuecheng.ucenter.model.dto.FindPasswordDto;
import com.xuecheng.ucenter.model.dto.RegisterDto;
import com.xuecheng.ucenter.service.FindPwdService;
import com.xuecheng.ucenter.service.RegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "用户注册及找回密码")
@Slf4j
@RestController
public class UserController {

    @Autowired
    private XcUserMapper userMapper;

    @Autowired
    private FindPwdService findPwdService;

    @Autowired
    private RegisterService registerService;

    @ApiOperation(value = "找回密码")
    @PostMapping("/findpassword")
    public Boolean findpassword(@RequestBody FindPasswordDto findPasswordDto) {
        return findPwdService.findpassword(findPasswordDto);
    }

    @ApiOperation(value = "注册", tags = "注册")
    @PostMapping("/register")
    public void register(@RequestBody RegisterDto registerDto) {
        registerService.register(registerDto);
    }

}
