package com.xuecheng.auth.controller;

import com.xuecheng.ucenter.model.po.XcUser;
import com.xuecheng.ucenter.service.impl.WxAuthServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Api(value = "微信扫码登录")
@Slf4j
@Controller
public class WxLoginController {

    @Autowired
    WxAuthServiceImpl wxAuthService;

    @ApiOperation(value = "微信扫码登录回调")
    @RequestMapping("/wxLogin")
    public String wxLogin(String code, String state) {
        // 打印日志
        log.debug("微信扫码回调,code:{},state:{}", code, state);
        // 调用认证方法
        XcUser xcUser = wxAuthService.wxAuth(code);
        // 认证失败
        if (xcUser == null) {
            return "redirect:http://localhost/error.html";
        }
        // 认证成功，获取用户名
        String username = xcUser.getUsername();
        // 携带用户名和认证方式，重定向
        return "redirect:http://www.51xuecheng.cn/sign.html?username=" + username + "&authType=wx";
    }
}
