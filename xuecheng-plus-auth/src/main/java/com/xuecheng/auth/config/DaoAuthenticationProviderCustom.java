package com.xuecheng.auth.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * 自定义DaoAuthenticationProvider
 * ----------------------------------------------
 * 自定义的原因：
 * loadUserByUsername是由DaoAuthentication调用的，
 * DaoAuthenticationProvider 会进行密码校验，
 * 但是并不是所有的校验方式都需要密码（例如手机号验证码登录），所以我们现在需要重写一个
 * 现在重新定义DaoAuthenticationProviderCustom类，重写类的additionalAuthenticationChecks方法
 */
@Slf4j
@Component
public class DaoAuthenticationProviderCustom extends DaoAuthenticationProvider {

    // 由于DaoAuthenticationProvider调用UserDetailsService，所以这里需要注入一个
    @Autowired
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        // 覆盖父类中的UserDetailsService
        super.setUserDetailsService(userDetailsService);
    }

    // 屏蔽密码对比
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        // 里面啥也不写就不会校验密码了
    }
}
