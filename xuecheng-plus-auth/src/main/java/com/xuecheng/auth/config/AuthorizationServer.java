package com.xuecheng.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import javax.annotation.Resource;

/**
 * 授权服务器配置
 * 用 @Configuration 和 @EnableAuthorizationServer 注解来启用授权服务器功能
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

    @Resource(name = "authorizationServerTokenServicesCustom")
    private AuthorizationServerTokenServices authorizationServerTokenServices;

    @Autowired
    private AuthenticationManager authenticationManager;

    // 配置客户端详情服务（允许哪些客户端接入我们的认证服务）
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
                // 使用in-memory存储
                .inMemory()
                // client_id
                .withClient("XcWebApp")
                // 客户端密钥
                .secret(new BCryptPasswordEncoder().encode("XcWebApp"))
                // 资源列表
                .resourceIds("xuecheng-plus")
                // 该client允许的授权类型
                .authorizedGrantTypes("authorization_code", "password", "client_credentials", "implicit", "refresh_token")
                // 允许的授权范围
                .scopes("all")
                // false跳转到授权页面
                .autoApprove(false)
                // 客户端接收授权码的重定向地址
                .redirectUris("http://www.51xuecheng.cn");
    }


    // 配置令牌的访问端点和令牌服务
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                // 认证管理器（令牌端点）
                .authenticationManager(authenticationManager)
                // 令牌管理服务（令牌服务）
                .tokenServices(authorizationServerTokenServices)
                // 仅允许使用POST请求访问令牌端点
                .allowedTokenEndpointRequestMethods(HttpMethod.POST);
    }

    // 配置令牌端点的安全约束
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security
                .tokenKeyAccess("permitAll()")                 // oauth/token_key是公开
                .checkTokenAccess("permitAll()")               // oauth/check_token公开
                .allowFormAuthenticationForClients();          // 表单认证（申请令牌）
    }

}
