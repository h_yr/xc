package com.xuecheng.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;

/**
 * 令牌策略配置类
 */
@Configuration
public class TokenConfig {

    // 密钥，用于JWT令牌的签名
    private String SIGNING_KEY = "mq123";

    // 令牌的存储策略，用于管理OAUth2认证令牌的存储和访问
    @Autowired
    private TokenStore tokenStore;

    // JWT令牌转换器，设置签名密钥
    @Autowired
    private JwtAccessTokenConverter accessTokenConverter;


    /* 配置令牌存储策略
     * 这里使用JWT令牌存储
     * */
    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    /* 配置JWT令牌转换器
     * 用于JWT令牌的签名
     * */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        // 创建JWT令牌转换器
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        // 对JWT令牌进行签名
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }

    /* 配置令牌管理服务
     * 包括支持刷新令牌、令牌存储策略、令牌增强链等参数设置
     * */
    @Bean(name = "authorizationServerTokenServicesCustom")
    public AuthorizationServerTokenServices tokenService() {
        // 创建默认的令牌管理服务对象（Spring Security OAuth2提供）
        DefaultTokenServices service = new DefaultTokenServices();
        // 支持刷新令牌
        service.setSupportRefreshToken(true);
        // 令牌存储策略
        service.setTokenStore(tokenStore);

        // 创建令牌增强链对象
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        // 将accessTokenConverter添加到令牌增强链中
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(accessTokenConverter));
        // 将增强链设置到令牌管理服务中，以便生产令牌时应用增强操作
        service.setTokenEnhancer(tokenEnhancerChain);

        // 令牌默认有效期2小时
        service.setAccessTokenValiditySeconds(7200);
        // 刷新令牌默认有效期3天
        service.setRefreshTokenValiditySeconds(259200);
        return service;
    }
}