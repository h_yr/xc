package com.xuecheng.messagesdk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.messagesdk.model.po.MqMessageHistory;

/**
 * 消息历史SDK服务类
 */
public interface MqMessageHistoryService extends IService<MqMessageHistory> {

}
