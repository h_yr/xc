package com.xuecheng.messagesdk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.messagesdk.model.po.MqMessage;

import java.util.List;

/**
 * 消息SDK服务类
 */
public interface MqMessageService extends IService<MqMessage> {

    /**
     * 扫描消息表记录，采用与扫描视频处理表相同的思路
     *
     * @param shardIndex  分片序号
     * @param shardTotal  分片总数
     * @param messageType 消息类型
     * @param count       扫描记录数
     * @return java.util.List 消息记录
     */
    List<MqMessage> getMessageList(int shardIndex, int shardTotal, String messageType, int count);


    /**
     * 添加消息
     *
     * @param messageType  消息类型
     * @param businessKey1 业务id1（自定义，类如课程id）
     * @param businessKey2 业务id1（自定义，类如课程id）
     * @param businessKey3 业务id1（自定义，类如课程id）
     * @return 消息记录
     */
    MqMessage addMessage(String messageType, String businessKey1, String businessKey2, String businessKey3);


    /**
     * 完成任务
     *
     * @param id 消息id
     * @return int 更新成功：1
     */
    int completed(long id);


    /**
     * 完成阶段任务
     *
     * @param id 消息id
     * @return 更新成功：1
     */
    int completedStageOne(long id);

    int completedStageTwo(long id);

    int completedStageThree(long id);

    int completedStageFour(long id);


    /**
     * 查询阶段状态
     *
     * @param id 消息id
     * @return 阶段1-4处理状态, 0:初始，1:成功
     */
    int getStageOne(long id);

    int getStageTwo(long id);

    int getStageThree(long id);

    int getStageFour(long id);

}
