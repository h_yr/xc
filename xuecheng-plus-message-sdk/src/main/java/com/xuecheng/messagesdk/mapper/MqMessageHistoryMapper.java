package com.xuecheng.messagesdk.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.messagesdk.model.po.MqMessageHistory;

public interface MqMessageHistoryMapper extends BaseMapper<MqMessageHistory> {

}
