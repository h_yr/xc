package com.xuecheng.learning.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.learning.model.po.XcCourseTables;

public interface XcCourseTablesMapper extends BaseMapper<XcCourseTables> {

}
