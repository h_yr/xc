package com.xuecheng.learning.service;

import com.xuecheng.base.model.PageResult;
import com.xuecheng.learning.model.dto.MyCourseTableParams;
import com.xuecheng.learning.model.dto.XcChooseCourseDto;
import com.xuecheng.learning.model.dto.XcCourseTablesDto;
import com.xuecheng.learning.model.po.XcCourseTables;

public interface MyCourseTablesService {

    /**
     * 添加选课
     *
     * @param userId   用户id
     * @param courseId 课程id
     * @return XcChooseCourseDto
     */
    XcChooseCourseDto addChooseCourse(String userId, Long courseId);

    /**
     * 查询学习资格
     *
     * @param userId   用户id
     * @param courseId 课程id
     * @return XcCourseTablesDto
     */
    XcCourseTablesDto getLearnStatus(String userId, Long courseId);

    /**
     * 查询课程表
     *
     * @param params 课程表查询参数
     * @return PageResult<XcCourseTables>
     */
    PageResult<XcCourseTables> myCourseTable(MyCourseTableParams params);
}
