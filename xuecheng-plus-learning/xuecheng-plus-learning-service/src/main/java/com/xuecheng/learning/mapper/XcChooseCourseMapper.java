package com.xuecheng.learning.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.learning.model.po.XcChooseCourse;

public interface XcChooseCourseMapper extends BaseMapper<XcChooseCourse> {

}
