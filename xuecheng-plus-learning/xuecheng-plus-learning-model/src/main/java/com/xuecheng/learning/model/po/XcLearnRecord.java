package com.xuecheng.learning.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel("学习记录")
@Data
@TableName("xc_learn_record")
public class XcLearnRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @ApiModelProperty("课程id")
    private Long courseId;


    @ApiModelProperty("课程名称")
    private String courseName;


    @ApiModelProperty("用户id")
    private String userId;


    @ApiModelProperty("最近学习时间")
    private LocalDateTime learnDate;


    @ApiModelProperty("学习时长")
    private Long learnLength;


    @ApiModelProperty("章节id")
    private Long teachplanId;


    @ApiModelProperty("章节名称")
    private String teachplanName;

}
