package com.xuecheng.learning.model.po;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel("课程表")
@Data
@TableName("xc_course_tables")
public class XcCourseTables implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @ApiModelProperty("选课记录id")
    private Long chooseCourseId;


    @ApiModelProperty("用户id")
    private String userId;


    @ApiModelProperty("课程id")
    private Long courseId;


    @ApiModelProperty("机构id")
    private Long companyId;


    @ApiModelProperty("课程名称")
    private String courseName;


    @ApiModelProperty("课程类型")
    private String courseType;


    @ApiModelProperty("添加时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;


    @ApiModelProperty("开始服务时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime validtimeStart;


    @ApiModelProperty("到期时间")
    private LocalDateTime validtimeEnd;


    @ApiModelProperty("更新时间")
    private LocalDateTime updateDate;


    @ApiModelProperty("备注")
    private String remarks;

}
