package com.xuecheng.learning.model.po;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel("选课表")
@Data
@TableName("xc_choose_course")
public class XcChooseCourse implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @ApiModelProperty("课程id")
    private Long courseId;


    @ApiModelProperty("课程名称")
    private String courseName;


    @ApiModelProperty("用户id")
    private String userId;


    @ApiModelProperty("机构id")
    private Long companyId;


    @ApiModelProperty("选课类型")
    private String orderType;


    @ApiModelProperty("添加时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;


    @ApiModelProperty("课程有效期(天)")
    private Integer validDays;

    @ApiModelProperty("课程价格")
    private Float coursePrice;


    @ApiModelProperty("选课状态")
    private String status;


    @ApiModelProperty("开始服务时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime validtimeStart;


    @ApiModelProperty("结束服务时间")
    private LocalDateTime validtimeEnd;


    @ApiModelProperty("备注")
    private String remarks;

}
