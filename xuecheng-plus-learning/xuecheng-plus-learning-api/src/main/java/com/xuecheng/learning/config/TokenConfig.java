package com.xuecheng.learning.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * 用于对令牌进行校验，防止令牌被篡改
 */
@Configuration
public class TokenConfig {

    // JWT签名密钥，与认证服务保持一致
    private String SIGNING_KEY = "mq123";

    /*
     * 配置令牌存储策略
     * */
    @Bean
    public TokenStore tokenStore() {
        // JWT令牌存储方案
        return new JwtTokenStore(accessTokenConverter());
    }


    /*
     * 令牌转换器
     * 对JWT令牌进行签名
     * */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }

}
