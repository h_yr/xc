package com.xuecheng.search.service;


/**
 * 课程索引service
 */
public interface IndexService {

    /**
     * 添加文档到索引
     *
     * @param indexName 索引名称
     * @param id        主键
     * @param object    索引对象（需要操作的信息数据）
     * @return true表示成功，false失败
     */
    Boolean addCourseIndex(String indexName, String id, Object object);


    /**
     * 更新文档到索引
     *
     * @param indexName 索引名称
     * @param id        主键
     * @param object    索引对象（需要操作的信息数据）
     * @return true表示成功，false失败
     */
    Boolean updateCourseIndex(String indexName, String id, Object object);


    /**
     * 删除索引中指定的文档
     *
     * @param indexName 索引名称
     * @param id        主键
     * @return true表示成功，false失败
     */
    Boolean deleteCourseIndex(String indexName, String id);

}
