package com.xuecheng.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.search.po.CourseIndex;
import com.xuecheng.search.service.IndexService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 课程索引管理接口实现
 */
@Slf4j
@Service
public class IndexServiceImpl implements IndexService {

    /*注入ES客户端*/
    @Autowired
    RestHighLevelClient client;

    /**
     * 添加文档到索引
     *
     * @param indexName 索引名称
     * @param id        主键
     * @param object    索引对象（需要操作的信息数据）
     * @return true表示成功，false失败
     */
    @Override
    public Boolean addCourseIndex(String indexName, String id, Object object) {
        // 插入文档，创建索引请求对象（POST /索引名称/_doc/文档id）
        IndexRequest indexRequest = new IndexRequest(indexName).id(id);
        // 指定索引文档内容（JSON文档）
        indexRequest.source(JSON.toJSONString(object), XContentType.JSON);
        // 索引响应对象
        IndexResponse indexResponse = null;
        try {
            // 发送请求
            indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("添加文档到索引出错：文档id为{}，错误信息为{}", id, e.getMessage());
            XueChengPlusException.cast("添加文档到索引出错");
        }
        // 获取结果
        String name = indexResponse.getResult().name();
        System.out.println(name);
        // 返回结果
        return name.equalsIgnoreCase("created") || name.equalsIgnoreCase("updated");
    }


    /**
     * 更新文档到索引
     *
     * @param indexName 索引名称
     * @param id        主键
     * @param object    索引对象（需要操作的信息数据）
     * @return true表示成功，false失败
     */
    @Override
    public Boolean updateCourseIndex(String indexName, String id, Object object) {
        // 修改文档(全量修改)，创建索引请求对象（POST /索引名称/_doc/文档id）
        UpdateRequest updateRequest = new UpdateRequest(indexName, id);
        // 指定索引文档内容（JSON文档）
        updateRequest.doc(JSON.toJSONString(object), XContentType.JSON);
        // 索引响应对象
        UpdateResponse updateResponse = null;
        try {
            // 发送请求
            updateResponse = client.update(updateRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("更新文档到索引出错：文档id为{}，错误信息为{}", id, e.getMessage());
            XueChengPlusException.cast("更新文档到索引出错");
        }
        // 获取结果
        DocWriteResponse.Result result = updateResponse.getResult();
        // 返回结果
        return result.name().equalsIgnoreCase("updated");
    }


    /**
     * 删除索引中指定的文档
     *
     * @param indexName 索引名称
     * @param id        主键
     * @return true表示成功，false失败
     */
    @Override
    public Boolean deleteCourseIndex(String indexName, String id) {
        // 删除文档，创建索引请求对象（DELETE /索引名称/_doc/文档id）
        DeleteRequest deleteRequest = new DeleteRequest(indexName, id);
        // 响应对象
        DeleteResponse deleteResponse = null;
        try {
            // 发送请求
            deleteResponse = client.delete(deleteRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("从索引删除文档出错：文档id为{}，错误信息为{}", id, e.getMessage());
            XueChengPlusException.cast("从索引删除文档出错");
        }
        // 获取响应结果
        DocWriteResponse.Result result = deleteResponse.getResult();
        // 返回结果
        return result.name().equalsIgnoreCase("deleted");
    }
}
