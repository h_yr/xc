package com.xuecheng.search.controller;

import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.search.po.CourseIndex;
import com.xuecheng.search.service.IndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "课程信息索引接口", tags = "课程信息索引接口")
@RestController
@RequestMapping("/index")
public class CourseIndexController {

    // 索引名（即MySQL中的表）
    @Value("${elasticsearch.course.index}")
    private String courseIndexStore;

    @Autowired
    private IndexService indexService;

    @ApiOperation("添加课程信息到索引")
    @PostMapping("/course")
    public Boolean add(@RequestBody CourseIndex courseIndex) {
        // 获取课程id
        Long id = courseIndex.getId();
        // 判断课程id是否为空
        if (id == null) {
            XueChengPlusException.cast("课程id为空");
        }
        // 添加课程索引
        Boolean result = indexService.addCourseIndex(courseIndexStore, String.valueOf(id), courseIndex);
        // 判断是否添加成功
        if (!result) {
            XueChengPlusException.cast("添加课程索引失败");
        }
        // 返回结果
        return result;

    }
}
