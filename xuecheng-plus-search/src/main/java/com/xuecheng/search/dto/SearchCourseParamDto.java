package com.xuecheng.search.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@ApiModel("搜索课程参数DTO")
public class SearchCourseParamDto {

    @ApiModelProperty("关键字")
    private String keywords;

    @ApiModelProperty("大分类")
    private String mt;

    @ApiModelProperty("小分类")
    private String st;

    @ApiModelProperty("难度等级")
    private String grade;

}
