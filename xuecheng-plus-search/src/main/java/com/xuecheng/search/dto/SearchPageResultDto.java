package com.xuecheng.search.dto;

import com.xuecheng.base.model.PageResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@ApiModel("搜索结果分页DTO")
public class SearchPageResultDto<T> extends PageResult {

    @ApiModelProperty("大分类列表")
    List<String> mtList;

    @ApiModelProperty("小分类列表")
    List<String> stList;

    public SearchPageResultDto(List<T> items, long counts, long page, long pageSize) {
        super(items, counts, page, pageSize);
    }

}
