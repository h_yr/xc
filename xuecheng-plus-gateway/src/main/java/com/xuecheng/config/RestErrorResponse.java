package com.xuecheng.config;

import lombok.Data;

import java.io.Serializable;

/**
 * 错误响应参数包装
 */
@Data
public class RestErrorResponse implements Serializable {

    // 错误消息
    private String errMessage;

    public RestErrorResponse(String errMessage) {
        this.errMessage = errMessage;
    }

}
