package com.xuecheng.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * 用于校验令牌，防止令牌被篡改
 */
@Configuration
public class TokenConfig {

    // 密钥，用于对令牌进行签名
    String SIGNING_KEY = "mq123";

    // 令牌仓库，用于管理OAUth2认证令牌的存储和访问
    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    // 令牌转换器，用于使用密钥对令牌加密
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        // 创建令牌转换器对象
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        // 使用密钥对令牌进行加密
        converter.setSigningKey(SIGNING_KEY);
        // 返回
        return converter;
    }

}
