package com.xuecheng.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * 安全配置类
 */
@EnableWebFluxSecurity
@Configuration
public class SecurityConfig {

    // 安全拦截配置
    @Bean
    public SecurityWebFilterChain webFluxSecurityFilterChain(ServerHttpSecurity http) {

        return http.authorizeExchange()               // 开始定义安全拦截规则
                .pathMatchers("/**").permitAll()    // 表示对所有路径都允许访问
                .anyExchange().authenticated()        // 除了之前定义的路径，其他所有路径都需要认证
                .and().csrf().disable().build();      // 禁用跨站请求伪造保护，并构建SecurityWebFilterChain对象
    }

}
