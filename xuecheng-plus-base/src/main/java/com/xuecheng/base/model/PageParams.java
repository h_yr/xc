package com.xuecheng.base.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 分页查询通用参数
 * 用于接收分页条件
 */
@Data
@ToString
@ApiModel("分页查询通用参数")
public class PageParams {

    @ApiModelProperty("当前页码")
    private Long pageNo;

    @ApiModelProperty("每页记录数")
    private Long pageSize;

    public PageParams() {
    }

    public PageParams(Long pageNo, Long pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }
}
