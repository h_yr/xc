package com.xuecheng.base.exception;


import lombok.Getter;

/**
 * 自定义项目异常
 */
@Getter
public class XueChengPlusException extends RuntimeException {
    private String errMessage;

    public XueChengPlusException(String message) {
        super(message);
        errMessage = message;
    }

    public static void cast(String errMessage) {
        throw new XueChengPlusException(errMessage);
    }
}
