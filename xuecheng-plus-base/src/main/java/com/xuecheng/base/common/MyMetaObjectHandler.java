package com.xuecheng.base.common;


import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 自定义元数据对象处理器
 * 在此类中统一为公共字段赋值
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入数据时自动填充字段
     *
     * @param metaObject 元数据
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        // 设置默认创建时间为当前时间
        metaObject.setValue("createDate", LocalDateTime.now());
        // 设置默认更新时间为当前时间
//        metaObject.setValue("changeDate", LocalDateTime.now());
    }

    /**
     * 更新数据时自动填充字段
     *
     * @param metaObject 元数据
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        // 设置默认更新时间为当前时间
        metaObject.setValue("changeDate", LocalDateTime.now());
    }
}
